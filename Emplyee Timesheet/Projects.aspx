﻿<%@ Page Title="Projects" Language="vb" EnableSessionState="True" AutoEventWireup="false" MasterPageFile="~/Layout.Master" CodeBehind="Projects.aspx.vb" Inherits="Test_application.Projects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form" runat="server">
        <table border="1" style="border-collapse: collapse">
        </table>
        <table class="auto-style1">
            <tr>
                <td style="text-align:center"><b>Add Project</b></td>
                <td style="text-align:center">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <table class="auto-style3">
                        <tr>
                            <td class="auto-style4">
                                <asp:Label ID="lblPname" runat="server" Text="Project Name"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <asp:TextBox ID="txtprojectName" runat="server" Width="340px" />
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                                <asp:Label ID="lblCname" runat="server" Text="Client name"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtclientName" runat="server" Width="339px"/>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp; &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                            <td>
                                <asp:Button ID="btnAdd" runat="server" Text="ADD" OnClick="Insert" Height="34px" Width="155px" />
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style6">
                    <br />
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT * FROM [tbl_projects]"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
    </form>
</asp:Content>
