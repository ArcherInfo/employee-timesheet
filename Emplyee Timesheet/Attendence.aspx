﻿<%@ Page Title="Attendance" Language="vb" AutoEventWireup="false" MasterPageFile="~/Layout.Master" CodeBehind="Attendence.aspx.vb" Inherits="Test_application.Attendence" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form4" runat="server">
        <table class="auto-style1">
            <tr>
                <td class="auto-style5" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label2" runat="server" style="text-align: center" Text="ATTENDENCE"></asp:Label>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <table class="auto-style3">
                        <tr>
                            <td class="auto-style14">
                                <asp:Label ID="lbl_uid" runat="server" Text="User ID"></asp:Label>
                            </td>
                            <td>
                    <asp:TextBox ID="txt_user_id" runat="server" Width="340px" Height="28px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style14">
                                <asp:Label ID="lbl_dateTime" runat="server" Text="Date-Time"></asp:Label>
                            </td>
                            <td>
                    <asp:TextBox ID="txt_date_time" runat="server" Width="339px" Height="33px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style15">
                                &nbsp;</td>
                            <td class="auto-style16">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">
                    <asp:Button ID="btnAdd3" runat="server" Text="ADD" OnClick="Insert" Height="34px" Width="155px" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style12">
        <asp:GridView ID="GridView13" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnRowDataBound"
            DataKeyNames="attendence_id" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit" PageSize = "3" AllowPaging ="True" OnPageIndexChanging = "OnPaging"
            OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" EmptyDataText="No records has been added."
            Width="468px" Height="202px" style="margin-left: 0px" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
            <Columns>
                <asp:BoundField DataField="attendence_id" HeaderText="Attendence Id" InsertVisible="False" ReadOnly="True" SortExpression="attendence_id" />
                <asp:BoundField DataField="user_id" HeaderText="User Id" SortExpression="user_id" />
                <asp:BoundField DataField="datetime" HeaderText="Date Time" SortExpression="datetime" />
                <asp:CommandField ShowEditButton="True" HeaderText="Actions" ShowDeleteButton="True" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT * FROM [tbl_attendence]"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
    <div>
    
    </div>
    </form>
</asp:Content>   