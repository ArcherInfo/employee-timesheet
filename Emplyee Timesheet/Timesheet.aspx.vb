﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient

Public Class Timesheet
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim query As String = "SELECT * FROM tbl_timesheet"
        Using con As SqlConnection = New SqlConnection(constr)
            Using sda As SqlDataAdapter = New SqlDataAdapter(query, con)
                Using dt As DataTable = New DataTable()
                    sda.Fill(dt)
                    GridView13.DataSourceID = ""
                    GridView13.DataSource = dt
                    GridView13.DataBind()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Insert(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd3.Click
        Dim Date1 As String = txt_date.Text
        Dim Time As String = txt_time.Text
        Dim proid = project_Id.SelectedItem.Value
        'Dim project_id As Integer = Convert.ToInt32(ToString)
        'Dim user_id As Integer = Convert.ToInt32(ToString)
        'Dim project_id As String = project_id.Text
        'Dim user_id As String = user_id.Text
        Dim query As String = "INSERT INTO tbl_timesheet VALUES(@date, @time,@pro_id,@user_id)"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        txt_date.Text = ""
        txt_time.Text = ""
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@date", Date1)
                cmd.Parameters.AddWithValue("@time", Time)
                cmd.Parameters.AddWithValue("@pro_id", proid)
                cmd.Parameters.AddWithValue("@user_id", 2)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        Me.BindGrid()
    End Sub

    Protected Sub OnRowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Dim row As GridViewRow = GridView13.Rows(e.RowIndex)
        Dim timesheet_id As Integer = Convert.ToInt32(GridView13.DataKeys(e.RowIndex).Values(0))
        Dim Date1 As String = (TryCast(row.FindControl("txt_date"), TextBox)).Text
        Dim time As String = (TryCast(row.FindControl("txt_time"), TextBox)).Text
        Dim project_id As Integer = (TryCast(row.FindControl("project_Id"), DropDownList)).Text
        '  Dim user_name As String = (TryCast(row.FindControl("user_Id"), TextBox)).Text
        Dim query As String = "UPDATE tbl_timesheet SET Date1=@date, Time=@time, project_name=@pro_name,user_name=@user_name WHERE timesheet_id=@timesheet_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@timesheet_id", timesheet_id)
                cmd.Parameters.AddWithValue("@txt_time", Date1)
                cmd.Parameters.AddWithValue("@time", time)
                cmd.Parameters.AddWithValue("@pro_name", project_id)
                'cmd.Parameters.AddWithValue("@user_name", user_name)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        GridView13.EditIndex = -1
        Me.BindGrid()
    End Sub

    Protected Sub OnRowCancelingEdit(sender As Object, e As EventArgs)
        GridView13.EditIndex = -1
        Me.BindGrid()
    End Sub

    Protected Sub OnRowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        Dim timesheet_id As Integer = Convert.ToInt32(GridView13.DataKeys(e.RowIndex).Values(0))
        Dim query As String = "DELETE FROM tbl_timesheet WHERE timesheet_id=@timesheet_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@timesheet_id", timesheet_id)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        Me.BindGrid()
    End Sub

    Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow AndAlso e.Row.RowIndex <> GridView13.EditIndex Then
        '    TryCast(e.Row.Cells(6).Controls(6), LinkButton).Attributes("onclick") = "return confirm('Do you want to delete this row?');"
        'End If
    End Sub
    Protected Sub OnRowEditing(sender As Object, e As GridViewEditEventArgs)
        GridView13.EditIndex = e.NewEditIndex
        Me.BindGrid()
    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        GridView13.PageIndex = e.NewPageIndex
        Me.BindGrid()
    End Sub

End Class