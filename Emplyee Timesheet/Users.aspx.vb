﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Public Class Users
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub
   

    Protected Sub Insert(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd2.Click
        Dim Name As String = txtUName.Text
        Dim Address As String = txtUAddress.Text
        Dim Dob As String = txtdob.Text
        Dim Contact As String = ""

        Dim midName As String = txtmid.Text()
        Dim lastName As String = txtlast.Text
        Dim gender As String = Male.Text
        Dim city As String = txtCity.Text
        Dim state As String = dropstatelist.SelectedValue.ToString
        Dim pinCode As String = txtpin.Text
        Dim email As String = txtemail.Text

        Dim query As String = "INSERT INTO tbl_user2 VALUES(@name,@address,@dob,@cno,@midNm,@lastnm,@gen,@city,@state,@pincd,@email)"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        txtUName.Text = ""
        txtUAddress.Text = ""
        txtmid.Text = ""
        txtdob.Text = ""
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@name", Name)
                cmd.Parameters.AddWithValue("@address", Address)
                cmd.Parameters.AddWithValue("@dob", Dob)
                cmd.Parameters.AddWithValue("@cno", Contact)

                cmd.Parameters.AddWithValue("@midNm", midName)
                cmd.Parameters.AddWithValue("@lastnm", lastName)
                cmd.Parameters.AddWithValue("@gen", gender)
                cmd.Parameters.AddWithValue("@city", city)
                cmd.Parameters.AddWithValue("@state", state)
                cmd.Parameters.AddWithValue("@pincd", pinCode)
                cmd.Parameters.AddWithValue("@email", email)

                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using
    End Sub
End Class