﻿<%@ Page Title="Users" Language="vb" AutoEventWireup="false" MasterPageFile="~/Layout.Master" CodeBehind="Users.aspx.vb" Inherits="Test_application.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 16px;
        }
        .auto-style2 {
            height: 20px;
        }
        .auto-style5 {
            width: 44px;
        }
        .auto-style6 {
            width: 1255px;
        }
        .auto-style7 {
            width: 83px;
        }
        .auto-style9 {
            height: 70px;
            width: 1241px;
        }
        .auto-style10 {
            height: 70px;
            width: 14px;
        }
        .auto-style14 {
            height: 22px;
        }
        .auto-style17 {
            height: 22px;
            width: 54px;
        }
        .auto-style19 {
            height: 20px;
            width: 54px;
        }
        .auto-style20 {
            height: 16px;
            width: 54px;
        }
        .auto-style22 {
            height: 70px;
            width: 54px;
        }
        .auto-style23 {
            width: 54px;
        }
        .auto-style24 {
            width: 1241px;
        }
        .auto-style25 {
            width: 76px;
        }
        .auto-style27 {
            height: 22px;
            width: 74px;
        }
        .auto-style29 {
            height: 20px;
            width: 74px;
        }
        .auto-style30 {
            height: 16px;
            width: 74px;
        }
        .auto-style31 {
            height: 70px;
            width: 74px;
        }
        .auto-style32 {
            width: 74px;
        }
        .auto-style33 {
            width: 204px;
        }
        .auto-style34 {
            height: 22px;
            width: 204px;
        }
        .auto-style36 {
            height: 20px;
            width: 204px;
        }
        .auto-style37 {
            height: 16px;
            width: 204px;
        }
        .auto-style38 {
            height: 70px;
            width: 204px;
        }
        .auto-style39 {
            height: 17px;
            width: 204px;
        }
        .auto-style40 {
            height: 17px;
        }
        .auto-style41 {
            height: 17px;
            width: 54px;
        }
        .auto-style42 {
            height: 17px;
            width: 74px;
        }
        .auto-style43 {
            width: 284px;
        }
        .auto-style44 {
            height: 22px;
            width: 284px;
        }
        .auto-style45 {
            height: 17px;
            width: 284px;
        }
        .auto-style46 {
            height: 20px;
            width: 284px;
        }
        .auto-style47 {
            height: 16px;
            width: 284px;
        }
        .auto-style59 {
            height: 31px;
            width: 204px;
        }
        .auto-style60 {
            height: 31px;
        }
        .auto-style62 {
            width: 284px;
            height: 31px;
        }
        .auto-style63 {
            width: 54px;
            height: 31px;
        }
        .auto-style64 {
            width: 74px;
            height: 31px;
        }
        .auto-style65 {
            width: 341px;
        }
        .auto-style66 {
            height: 22px;
            width: 341px;
        }
        .auto-style67 {
            height: 17px;
            width: 341px;
        }
        .auto-style68 {
            height: 20px;
            width: 341px;
        }
        .auto-style69 {
            height: 16px;
            width: 341px;
        }
        .auto-style71 {
            height: 31px;
            width: 341px;
        }
        .auto-style72 {
            width: 204px;
            height: 5px;
        }
        .auto-style73 {
            width: 341px;
            height: 5px;
        }
        .auto-style74 {
            height: 5px;
        }
        .auto-style76 {
            width: 284px;
            height: 5px;
        }
        .auto-style77 {
            width: 54px;
            height: 5px;
        }
        .auto-style78 {
            width: 74px;
            height: 5px;
        }
        .auto-style79 {
            width: 164px;
        }
        .auto-style80 {
            width: 340px;
        }
        .auto-style81 {
            height: 22px;
            width: 340px;
        }
        .auto-style82 {
            height: 17px;
            width: 340px;
        }
        .auto-style83 {
            width: 340px;
            height: 5px;
        }
        .auto-style84 {
            height: 16px;
            width: 340px;
            margin-left: 200px;
        }
        .auto-style86 {
            width: 340px;
            height: 31px;
        }
        .auto-style87 {
            height: 20px;
            width: 340px;
        }
        .auto-style89 {
            height: 22px;
            width: 76px;
        }
        .auto-style90 {
            height: 17px;
            width: 76px;
        }
        .auto-style91 {
            width: 76px;
            height: 5px;
        }
        .auto-style92 {
            height: 16px;
            width: 76px;
        }
        .auto-style93 {
            width: 76px;
            height: 31px;
        }
        .auto-style94 {
            height: 20px;
            width: 76px;
        }
        .auto-style95 {
            height: 70px;
            width: 76px;
        }
        .auto-style96 {
            width: 79px;
        }
        .auto-style97 {
            height: 70px;
            width: 79px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form" runat="server">
        <div>
            <table class="auto-style6">
                <tr>
                    <td style="text-align: center" class="auto-style24"><b>Add User</b></td>
                    <td style="text-align: center" class="auto-style96">&nbsp;</td>
                    </tr>
                <tr>
                    <td style="text-align: center" class="auto-style24">&nbsp;</td>
                    <td style="text-align: center" class="auto-style96">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <table class="auto-style9">
                            <tr>
                                <td class="auto-style33">
                                    <asp:Label ID="lblUserName" runat="server" Text="First Name"></asp:Label>
                                </td>
                                <td class="auto-style65" colspan="2">
                                    <asp:TextBox ID="txtUName" runat="server" Height="30px" Width="276px"></asp:TextBox>
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                <td class="auto-style80">&nbsp;<asp:Label ID="lblUserDOB" runat="server" Text="Middle Name" Height="16px"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp; </td>
                                <td class="auto-style43"><asp:TextBox ID="txtmid" runat="server" Height="27px" Width="276px"></asp:TextBox>
                                    </td>
                                <td class="auto-style25">&nbsp;</td>
                                <td class="auto-style32">
                                    <asp:Label ID="Label1" runat="server" Text="Last Name" Width="82px"></asp:Label>
                                </td>
                                <td class="auto-style23">
                                    <asp:TextBox ID="txtlast" runat="server" Height="32px" Width="276px"></asp:TextBox>
                                </td>
                                <td class="auto-style25">&nbsp;</td>
                                <td class="auto-style7">&nbsp;</td>
                                <td class="auto-style5">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style34"></td>
                                <td class="auto-style66" colspan="2"></td>
                                <td class="auto-style14"></td>
                                <td class="auto-style81"></td>
                                <td class="auto-style44">&nbsp;</td>
                                <td class="auto-style89"></td>
                                <td class="auto-style27"></td>
                                <td class="auto-style17">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style39">
                                    Gender</td>
                                <td class="auto-style67" colspan="2">
                                    <asp:RadioButton ID="Male" runat="server" GroupName="a" Text="Male" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:RadioButton ID="Female" runat="server" GroupName="a" Text="Female" />
                                </td>
                                <td class="auto-style40"></td>
                                <td class="auto-style82">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label2" runat="server" Text="DOB"></asp:Label>
                                </td>
                                <td class="auto-style45"><asp:TextBox ID="txtdob" Type="Date" runat="server" Height="27px" Width="276px"></asp:TextBox>
                                </td>
                                <td class="auto-style90">
                                </td>
                                <td class="auto-style42">
                                    &nbsp;</td>
                                <td class="auto-style41">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style72"></td>
                                <td class="auto-style73" colspan="2"></td>
                                <td class="auto-style74"></td>
                                <td class="auto-style83"></td>
                                <td class="auto-style76"></td>
                                <td class="auto-style91"></td>
                                <td class="auto-style78"></td>
                                <td class="auto-style77"></td>
                            </tr>
                            <tr>
                                <td class="auto-style37">
                                    <asp:Label ID="lblUserAdd" runat="server" Text="Address"></asp:Label>
                                    </td>
                                <td class="auto-style69" colspan="2">&nbsp;<asp:TextBox ID="txtUAddress" runat="server" Height="30px" Width="276px"></asp:TextBox>
                                </td>
                                <td class="auto-style1"></td>
                                <td class="auto-style84">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; City</td>
                                <td class="auto-style47">&nbsp;<asp:TextBox ID="txtCity" runat="server" Height="32px" Width="276px"></asp:TextBox>
                                </td>
                                <td class="auto-style92">&nbsp;</td>
                                <td class="auto-style30">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; State&nbsp;</td>
                                <td class="auto-style20">
                                    <asp:DropDownList ID="dropstatelist" runat="server" Height="30px" Width="276px">
                                        <asp:ListItem>Andra Pradesh</asp:ListItem>
                                        <asp:ListItem>Assam</asp:ListItem>
                                        <asp:ListItem>Bihar</asp:ListItem>
                                        <asp:ListItem>Chhattsgarh </asp:ListItem>
                                        <asp:ListItem>Maharashtra</asp:ListItem>
                                        <asp:ListItem>Karnataka</asp:ListItem>
                                        <asp:ListItem>Kerala</asp:ListItem>
                                        <asp:ListItem>Utter Pradesh</asp:ListItem>
                                        <asp:ListItem>Gujarat</asp:ListItem>
                                        <asp:ListItem>Goa</asp:ListItem>
                                        <asp:ListItem>Jammu</asp:ListItem>
                                        <asp:ListItem>Arunachal Pradesh</asp:ListItem>
                                        <asp:ListItem>Haryna</asp:ListItem>
                                        <asp:ListItem>Himachal Pradesh</asp:ListItem>
                                        <asp:ListItem>Jharkhand</asp:ListItem>
                                        <asp:ListItem>Madhya Pradesh</asp:ListItem>
                                        <asp:ListItem>Manipur</asp:ListItem>
                                        <asp:ListItem>Meghalaya</asp:ListItem>
                                        <asp:ListItem>Nagaland</asp:ListItem>
                                        <asp:ListItem>Odisha</asp:ListItem>
                                        <asp:ListItem>Panjab</asp:ListItem>
                                        <asp:ListItem>Rajasthan</asp:ListItem>
                                        <asp:ListItem>Sikkim</asp:ListItem>
                                        <asp:ListItem>Tamil Nadu</asp:ListItem>
                                        <asp:ListItem>Telangana</asp:ListItem>
                                        <asp:ListItem>Tripura</asp:ListItem>
                                        <asp:ListItem>Uttarakhand</asp:ListItem>
                                        <asp:ListItem>Wast Bengal</asp:ListItem>
                                        <asp:ListItem>Andaman &amp; Nikobar</asp:ListItem>
                                        <asp:ListItem>Chandigarh</asp:ListItem>
                                        <asp:ListItem>Dadar &amp; Nagarhaveli</asp:ListItem>
                                        <asp:ListItem>Daman &amp; Diu</asp:ListItem>
                                        <asp:ListItem>Delihi</asp:ListItem>
                                        <asp:ListItem>Laksha Dweep</asp:ListItem>
                                        <asp:ListItem>Poducherry</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style37"></td>
                                <td class="auto-style69" colspan="2"></td>
                                <td class="auto-style1"></td>
                                <td class="auto-style84"></td>
                                <td class="auto-style47"></td>
                                <td class="auto-style92">&nbsp;</td>
                                <td class="auto-style30">&nbsp;</td>
                                <td class="auto-style20">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style37">
                                    <asp:Label ID="Label4" runat="server" Text="Pin Code"></asp:Label>
                                </td>
                                <td class="auto-style69" colspan="2">
                                    <asp:TextBox ID="txtpin" runat="server" Height="30px" Width="276px"></asp:TextBox>
                                </td>
                                <td class="auto-style1"></td>
                                <td class="auto-style84">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label6" runat="server" Text="Email"></asp:Label>
                                &nbsp;</td>
                                <td class="auto-style47">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:TextBox ID="txtemail" type="Email" runat="server" Height="30px" Width="276px"></asp:TextBox>
                                    </td>
                                <td class="auto-style92">&nbsp;</td>
                                <td class="auto-style37">
                                    <asp:Label ID="lblUserCN" runat="server" Text="Contact No."></asp:Label>
                                </td>
                                <td class="auto-style20">
                                    <asp:TextBox ID="txtcnt" runat="server" Height="30px" Width="276px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style37">&nbsp;</td>
                                <td class="auto-style69" colspan="2">&nbsp;</td>
                                <td class="auto-style1">&nbsp;</td>
                                <td class="auto-style84">&nbsp;</td>
                                <td class="auto-style47">&nbsp;</td>
                                <td class="auto-style92">&nbsp;</td>
                                <td class="auto-style30">&nbsp;</td>
                                <td class="auto-style20">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style59">
                                    &nbsp;</td>
                                <td class="auto-style71" colspan="2">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemail" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </td>
                                <td class="auto-style60"></td>
                                <td class="auto-style86"></td>
                                <td class="auto-style62"></td>
                                <td class="auto-style93"></td>
                                <td class="auto-style64"></td>
                                <td class="auto-style63"></td>
                            </tr>
                            <tr>
                                <td class="auto-style38">&nbsp;</td>
                                <td class="auto-style79">

                                    <asp:Button ID="btnAdd2" runat="server" Text="ADD" OnClick="Insert" Height="40px" Width="140px" />
                                </td>
                                <td class="auto-style65">

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="Button1"  type="Reset" runat="server" Text="Reset" Height="40px" Width="140px" />
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td class="auto-style80">&nbsp;</td>
                                <td class="auto-style43">&nbsp;</td>
                                <td class="auto-style25">&nbsp;</td>
                                <td class="auto-style32">&nbsp;</td>
                                <td class="auto-style23">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style38">&nbsp;</td>
                                <td class="auto-style65" colspan="2">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="auto-style80">&nbsp;</td>
                                <td class="auto-style43">&nbsp;</td>
                                <td class="auto-style25">&nbsp;</td>
                                <td class="auto-style32">&nbsp;</td>
                                <td class="auto-style23">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center" class="auto-style36">&nbsp;</td>
                                <td class="auto-style68" colspan="2"></td>
                                <td class="auto-style2"></td>
                                <td class="auto-style87"></td>
                                <td class="auto-style46"></td>
                                <td class="auto-style94">&nbsp;</td>
                                <td class="auto-style29">&nbsp;</td>
                                <td class="auto-style19">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style38">&nbsp;</td>
                                <td class="auto-style65" colspan="2">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="auto-style80">&nbsp;</td>
                                <td class="auto-style43">
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT * FROM [tbl_user2]"></asp:SqlDataSource>
                                </td>
                                <td class="auto-style25">&nbsp;</td>
                                <td class="auto-style32">&nbsp;</td>
                                <td class="auto-style23">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style10" colspan="6">
                                    &nbsp;</td>
                                <td class="auto-style95">
                                    &nbsp;</td>
                                <td class="auto-style31">
                                    &nbsp;</td>
                                <td class="auto-style22">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td class="auto-style97">
                        &nbsp;</td>
                </tr>
            </table>

        </div>
    </form>
</asp:Content>
