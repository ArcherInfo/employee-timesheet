﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient

Public Class Leaves
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.BindGrid()
        End If
    End Sub


    Private Sub BindGrid()
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim query As String = "SELECT * FROM tbl_leaves"
        Using con As SqlConnection = New SqlConnection(constr)
            Using sda As SqlDataAdapter = New SqlDataAdapter(query, con)
                Using dt As DataTable = New DataTable()
                    sda.Fill(dt)
                    GridView14.DataSourceID = ""
                    GridView14.DataSource = dt
                    GridView14.DataBind()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Insert(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd3.Click
        Dim leave_date As String = txt_leave_date.Text
        Dim leave_type As String = DropDownList1.Text
        ' Dim user_id As String = txt_leave_uid.Text
        Dim query As String = "INSERT INTO tbl_leaves VALUES(@ldate, @ltype,@user_id)"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        'txt_leave_date.Text = ""
        'DropDownList1.Text = ""
        ' txt_leave_uid.Text = ""
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@ldate", leave_date)
                cmd.Parameters.AddWithValue("@ltype", leave_type)
                cmd.Parameters.AddWithValue("@user_id", 2)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
            MsgBox("Leave Applied Successfully")

        End Using

        Me.BindGrid()
    End Sub

    Protected Sub OnRowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Dim row As GridViewRow = GridView14.Rows(e.RowIndex)
        Dim attendence_id As Integer = Convert.ToInt32(GridView14.DataKeys(e.RowIndex).Values(0))
        Dim leave_date As String = (TryCast(row.FindControl("txt_leave_date"), TextBox)).Text
        Dim leave_type As String = (TryCast(row.FindControl("DropDownList1"), TextBox)).Text
        Dim user_id As String = (TryCast(row.FindControl("txt_leave_uid"), TextBox)).Text
        Dim query As String = "UPDATE tbl_attendence SET leave_date=@ldate, leave_type=@ltype,user_id=@user_id WHERE attendence_id=@attendence_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@attendence_id", attendence_id)
                cmd.Parameters.AddWithValue("@ldate", leave_date)
                cmd.Parameters.AddWithValue("@ltype", leave_type)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        GridView14.EditIndex = -1
        Me.BindGrid()
    End Sub

    Protected Sub OnRowCancelingEdit(sender As Object, e As EventArgs)
        GridView14.EditIndex = -1
        Me.BindGrid()
    End Sub

    Protected Sub OnRowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        Dim leave_id As Integer = Convert.ToInt32(GridView14.DataKeys(e.RowIndex).Values(0))
        Dim query As String = "DELETE FROM tbl_leaves WHERE leave_id=@leave_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@leave_id", leave_id)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        Me.BindGrid()
    End Sub

    Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow AndAlso e.Row.RowIndex <> GridView14.EditIndex Then
        '    TryCast(e.Row.Cells(2).Controls(2), LinkButton).Attributes("onclick") = "return confirm('Do you want to delete this row?');"
        'End If
    End Sub
    Protected Sub OnRowEditing(sender As Object, e As GridViewEditEventArgs)
        GridView14.EditIndex = e.NewEditIndex
        Me.BindGrid()
    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        GridView14.PageIndex = e.NewPageIndex
        Me.BindGrid()
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub
End Class