﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Public Class ListOfUsers
    Inherits System.Web.UI.Page
    Private Sub BindGrid()
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim query As String = "SELECT * FROM tbl_user2"
        Using con As SqlConnection = New SqlConnection(constr)
            Using sda As SqlDataAdapter = New SqlDataAdapter(query, con)
                Using dt As DataTable = New DataTable()
                    'dt.Columns.Add(New DataColumn("name", GetType(String)))
                    'dt.Columns.Add(New DataColumn("address", GetType(String)))
                    'dt.Columns.Add(New DataColumn("dob", GetType(Date)))
                    'dt.Columns.Add(New DataColumn("cno", GetType(Integer)))
                    sda.Fill(dt)
                    GridView12.DataSourceID = ""
                    GridView12.DataSource = dt
                    GridView12.DataBind()
                End Using
            End Using
        End Using
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.BindGrid()
        End If
    End Sub
    Protected Sub OnRowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Dim row As GridViewRow = GridView12.Rows(e.RowIndex)
        Dim user_id As Integer = Convert.ToInt32(GridView12.DataKeys(e.RowIndex).Values(0))
        Dim Name As String = (TryCast(row.FindControl("txtUName"), TextBox)).Text
        Dim Address As String = (TryCast(row.FindControl("txtUAddress"), TextBox)).Text
        Dim Dob As String = (TryCast(row.FindControl("txt_Dob"), TextBox)).Text
        Dim Contact As String = (TryCast(row.FindControl("txt_cno"), TextBox)).Text

        Dim query As String = "UPDATE tbl_user2 SET Name=@name, Address=@address, Dob=@dob, Contact=@cno WHERE user_id=@user_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)

                cmd.Parameters.AddWithValue("@user_id", user_id)
                cmd.Parameters.AddWithValue("@name", Name)
                cmd.Parameters.AddWithValue("@address", Address)
                cmd.Parameters.AddWithValue("@dob", Dob)
                cmd.Parameters.AddWithValue("@cno", Contact)

                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        GridView12.EditIndex = -1
        Me.BindGrid()
    End Sub

    Protected Sub OnRowCancelingEdit(sender As Object, e As EventArgs)
        GridView12.EditIndex = -1
        Me.BindGrid()
    End Sub

    Protected Sub OnRowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        Dim user_id As Integer = Convert.ToInt32(GridView12.DataKeys(e.RowIndex).Values(0))
        Dim query As String = "DELETE FROM tbl_user2 WHERE user_id=@user_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@user_id", user_id)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        Me.BindGrid()
    End Sub
    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        GridView12.PageIndex = e.NewPageIndex
        Me.BindGrid()
    End Sub
End Class