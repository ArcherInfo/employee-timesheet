﻿<%@ Page Title="Holidays" Language="vb" AutoEventWireup="false" MasterPageFile="~/Layout.Master" CodeBehind="Holidays.aspx.vb" Inherits="Test_application.Holidays" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 39px;
        }
        .auto-style2 {
            height: 62px;
        }
        .auto-style3 {
            height: 44px;
        }
        .auto-style4 {
            height: 59px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form" runat="server">
        <br />
        <table class="auto-style1">
            <tr>
                <td class="auto-style5" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label2" runat="server" style="text-align: center" Text="HOLIDAYS"></asp:Label>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <table class="auto-style3">
                        <tr>
                            <td class="auto-style2">
                                <asp:Label ID="lbl_Fh" runat="server" Text="First Half"></asp:Label>
                            </td>
                            <td class="auto-style2">
                    <asp:TextBox ID="txt_Fhalf" runat="server" Width="339px" Height="28px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                                <asp:Label ID="lbl_Sf" runat="server" Text="Second Half"></asp:Label>
                            </td>
                            <td class="auto-style4">
                    <asp:TextBox ID="txt_SHalf" runat="server" Width="339px" Height="33px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style15">
                                <asp:Label ID="lbl_Year" runat="server" Text="Year"></asp:Label>
                            </td>
                            <td class="auto-style16">
                                <asp:DropDownList ID="DropDownList1" runat="server">
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                    <asp:ListItem>2021</asp:ListItem>
                                    <asp:ListItem>2022</asp:ListItem>
                                    <asp:ListItem>2023</asp:ListItem>
                                    <asp:ListItem>2024</asp:ListItem>
                                    <asp:ListItem>2025</asp:ListItem>
                                    <asp:ListItem>2026</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">
                    <asp:Button ID="btnAdd3" runat="server" Text="ADD" OnClick="Insert" Height="34px" Width="155px" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style12">
        <asp:GridView ID="GridView15" runat="server" AutoGenerateColumns="False" OnRowEditing="OnRowEditing" PageSize = "3" AllowPaging ="True" OnPageIndexChanging = "OnPaging"
            EmptyDataText="No records has been added."
            Width="468px" Height="202px" style="margin-left: 0px" AllowSorting="True" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="first_half" HeaderText="first_half" SortExpression="first_half" />
                <asp:BoundField DataField="second_half" HeaderText="second_half" SortExpression="second_half" />
                <asp:BoundField DataField="year" HeaderText="year" SortExpression="year" />
                <asp:CommandField ShowEditButton="True" />
                <asp:CommandField ShowDeleteButton="True" />
            </Columns>
        </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT * FROM [tbl_Holidays]"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
    <div>
    
    </div>
    </form>
   </asp:Content>
