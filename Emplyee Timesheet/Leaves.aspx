﻿<%@ Page Title="Leaves" Language="vb" AutoEventWireup="false" MasterPageFile="~/Layout.Master" CodeBehind="Leaves.aspx.vb" Inherits="Test_application.Leaves" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form" runat="server">
        <br />
        <table class="auto-style1">
            <tr>
                <td class="auto-style5" colspan="2" style="text-align:center"><b>Apply Leave</b></td>
            </tr>
            <tr>
                <td class="auto-style5" colspan="2" style="text-align:center">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <table class="auto-style3">
                        <tr>
                            <td class="auto-style17">
                                <asp:Label ID="lbl_date" runat="server" Text="Leave Date"></asp:Label>
                            </td>
                            <td class="auto-style18">
                                <asp:TextBox ID="txt_leave_date" type="date" runat="server" Width="340px" Height="28px" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="auto-style17">&nbsp;</td>
                            <td class="auto-style18">&nbsp;</td>
                            <td>&nbsp;</td>
                            </tr>
                        <tr>
                            <td class="auto-style14">
                                <asp:Label ID="lbl_time" runat="server" Text="Leave Type"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownList1" runat="server" Width="340px">
                                    <asp:ListItem>Half Day</asp:ListItem>
                                    <asp:ListItem>Full Day</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">
                                <asp:Button ID="btnAdd3" runat="server" Text="ADD" OnClick="Insert" Height="34px" Width="155px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style15" colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style15" colspan="2" style="text-align:center"><b>List Of Leaves</b></td>
                        </tr>
                        <tr>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style15" colspan="2">
                                <asp:GridView ID="GridView14" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnRowDataBound"
                                    DataKeyNames="leave_id" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit" PageSize="3" AllowPaging="True" OnPageIndexChanging="OnPaging"
                                    OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" EmptyDataText="No records has been added."
                                    Width="675px" Height="100px" Style="margin-left: 0px" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                    <Columns>
                                        <asp:BoundField DataField="leave_id" HeaderText="Leave Id" InsertVisible="False" ReadOnly="True" SortExpression="leave_id" />
                                        <asp:BoundField DataField="leave_date" HeaderText="Leave Date" SortExpression="leave_date" />
                                        <asp:BoundField DataField="leave_type" HeaderText="Leave Type" SortExpression="leave_type" />
                                        <asp:BoundField DataField="user_id" HeaderText="User Id" SortExpression="user_id" />
                                        <asp:CommandField ShowEditButton="True" HeaderText="Actions" ShowDeleteButton="True" />
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style12">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT * FROM [tbl_leaves]"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
        <div>
        </div>
    </form>
</asp:Content>
