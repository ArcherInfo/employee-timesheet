﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient

Public Class Attendence
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.BindGrid()
        End If
    End Sub

    Private Sub BindGrid()
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim query As String = "SELECT * FROM tbl_Attendence"
        Using con As SqlConnection = New SqlConnection(constr)
            Using sda As SqlDataAdapter = New SqlDataAdapter(query, con)
                Using dt As DataTable = New DataTable()
                    sda.Fill(dt)
                    GridView13.DataSourceID = ""
                    GridView13.DataSource = dt
                    GridView13.DataBind()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Insert(ByVal sender As Object, ByVal e As EventArgs)
        Dim user_id As String = txt_user_id.Text
        Dim datetime As String = txt_date_time.Text
        Dim query As String = "INSERT INTO tbl_attendence VALUES(@user_id, @dt)"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        txt_user_id.Text = ""
        txt_date_time.Text = ""
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@user_id", user_id)
                cmd.Parameters.AddWithValue("@dt", datetime)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        Me.BindGrid()
    End Sub

    Protected Sub OnRowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Dim row As GridViewRow = GridView13.Rows(e.RowIndex)
        Dim attendence_id As Integer = Convert.ToInt32(GridView13.DataKeys(e.RowIndex).Values(0))
        Dim user_id As String = (TryCast(row.FindControl("txtName5"), TextBox)).Text
        Dim datetime As String = (TryCast(row.FindControl("txtCountry5"), TextBox)).Text
        Dim query As String = "UPDATE tbl_attendence SET user_id=@user_id, datetime=@dt WHERE attendence_id=@attendence_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@attendence_id", attendence_id)
                cmd.Parameters.AddWithValue("@user_id", user_id)
                cmd.Parameters.AddWithValue("@dt", datetime)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        GridView13.EditIndex = -1
        Me.BindGrid()
    End Sub

    Protected Sub OnRowCancelingEdit(sender As Object, e As EventArgs)
        GridView13.EditIndex = -1
        Me.BindGrid()
    End Sub

    Protected Sub OnRowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        Dim attendence_id As Integer = Convert.ToInt32(GridView13.DataKeys(e.RowIndex).Values(0))
        Dim query As String = "DELETE FROM tbl_attendence WHERE attendence_id=@attendence_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@attendence_id", attendence_id)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        Me.BindGrid()
    End Sub

    Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow AndAlso e.Row.RowIndex <> GridView13.EditIndex Then
        '    TryCast(e.Row.Cells(2).Controls(2), LinkButton).Attributes("onclick") = "return confirm('Do you want to delete this row?');"
        'End If
    End Sub
    Protected Sub OnRowEditing(sender As Object, e As GridViewEditEventArgs)
        GridView13.EditIndex = e.NewEditIndex
        Me.BindGrid()
    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        GridView13.PageIndex = e.NewPageIndex
        Me.BindGrid()
    End Sub

    Protected Sub txt_user_id_TextChanged(sender As Object, e As EventArgs)

    End Sub
End Class