﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Public Class ListOfProjects
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim query As String = "SELECT * FROM tbl_projects"
        Using con As SqlConnection = New SqlConnection(constr)
            Using sda As SqlDataAdapter = New SqlDataAdapter(query, con)
                Using dt As DataTable = New DataTable()
                    sda.Fill(dt)
                    GridView11.DataSourceID = ""
                    GridView11.DataSource = dt
                    GridView11.DataBind()
                End Using
            End Using
        End Using
    End Sub
    Protected Sub OnRowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Dim row As GridViewRow = GridView11.Rows(e.RowIndex)
        Dim project_id As Integer = Convert.ToInt32(GridView11.DataKeys(e.RowIndex).Values(0))
        Dim project_name As String = (TryCast(row.FindControl("txtprojectName"), TextBox)).Text
        Dim client_name As String = (TryCast(row.FindControl("txtclientName"), TextBox)).Text
        Dim query As String = "UPDATE tbl_projects SET project_name=@pname, client_name=@cname WHERE project_id=@project_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@project_id", project_id)
                cmd.Parameters.AddWithValue("@pname", project_name)
                cmd.Parameters.AddWithValue("@cname", client_name)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        GridView11.EditIndex = -1
        Me.BindGrid()
    End Sub
    Protected Sub OnRowCancelingEdit(sender As Object, e As EventArgs)
        GridView11.EditIndex = -1
        Me.BindGrid()
    End Sub

    Protected Sub OnRowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        Dim project_id As Integer = Convert.ToInt32(GridView11.DataKeys(e.RowIndex).Values(0))
        Dim query As String = "DELETE FROM tbl_projects WHERE project_id=@project_id"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@project_id", project_id)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        Me.BindGrid()
    End Sub


    Protected Sub OnRowEditing(sender As Object, e As GridViewEditEventArgs)
        GridView11.EditIndex = e.NewEditIndex
        Me.BindGrid()
    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        GridView11.PageIndex = e.NewPageIndex
        Me.BindGrid()
    End Sub
End Class