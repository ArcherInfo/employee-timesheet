﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient

Public Class Holidays
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim query As String = "SELECT * FROM tbl_Holidays"
        Using con As SqlConnection = New SqlConnection(constr)
            Using sda As SqlDataAdapter = New SqlDataAdapter(query, con)
                Using dt As DataTable = New DataTable()
                    sda.Fill(dt)
                    GridView15.DataSourceID = ""
                    GridView15.DataSource = dt
                    GridView15.DataBind()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Insert(ByVal sender As Object, ByVal e As EventArgs)
        Dim first_half As String = txt_Fhalf.Text
        Dim second_half As String = txt_SHalf.Text
        ' Dim year As String = txt_Year.Text
        Dim query As String = "INSERT INTO tbl_Holidays VALUES(@fh, @sh,@year)"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        txt_Fhalf.Text = ""
        txt_SHalf.Text = ""
        ' txt_Year.Text = ""
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@fh", first_half)
                cmd.Parameters.AddWithValue("@sh", second_half)
                ' cmd.Parameters.AddWithValue("@year", year)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        Me.BindGrid()
    End Sub

    'Protected Sub OnRowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
    '    Dim row As GridViewRow = GridView15.Rows(e.RowIndex)
    '    Dim project_id As Integer = Convert.ToInt32(GridView15.DataKeys(e.RowIndex).Values(0))
    '    Dim project_name As String = (TryCast(row.FindControl("txtName1"), TextBox)).Text
    '    Dim client_name As String = (TryCast(row.FindControl("txtCountry1"), TextBox)).Text
    '    Dim query As String = "UPDATE tbl_projects SET project_name=@pname, client_name=@cname WHERE project_id=@project_id"
    '    Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
    '    Using con As SqlConnection = New SqlConnection(constr)
    '        Using cmd As SqlCommand = New SqlCommand(query)
    '            cmd.Parameters.AddWithValue("@project_id", project_id)
    '            cmd.Parameters.AddWithValue("@pname", project_name)
    '            cmd.Parameters.AddWithValue("@cname", client_name)
    '            cmd.Connection = con
    '            con.Open()
    '            cmd.ExecuteNonQuery()
    '            con.Close()
    '        End Using
    '    End Using

    '    GridView11.EditIndex = -1
    '    Me.BindGrid()
    'End Sub

    'Protected Sub OnRowCancelingEdit(sender As Object, e As EventArgs)
    '    GridView11.EditIndex = -1
    '    Me.BindGrid()
    'End Sub

    'Protected Sub OnRowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
    '    Dim project_id As Integer = Convert.ToInt32(GridView11.DataKeys(e.RowIndex).Values(0))
    '    Dim query As String = "DELETE FROM tbl_projects WHERE project_id=@project_id"
    '    Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
    '    Using con As SqlConnection = New SqlConnection(constr)
    '        Using cmd As SqlCommand = New SqlCommand(query)
    '            cmd.Parameters.AddWithValue("@project_id", project_id)
    '            cmd.Connection = con
    '            con.Open()
    '            cmd.ExecuteNonQuery()
    '            con.Close()
    '        End Using
    '    End Using

    '    Me.BindGrid()
    'End Sub

    'Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
    '    If e.Row.RowType = DataControlRowType.DataRow AndAlso e.Row.RowIndex <> GridView11.EditIndex Then
    '        TryCast(e.Row.Cells(2).Controls(2), LinkButton).Attributes("onclick") = "return confirm('Do you want to delete this row?');"
    '    End If
    'End Sub

    Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow AndAlso e.Row.RowIndex <> GridView11.EditIndex Then
        '    TryCast(e.Row.Cells(3).Controls(3), LinkButton).Attributes("onclick") = "return confirm('Do You Want to delete this row? ');"
        'End If
    End Sub

    Protected Sub OnRowEditing(sender As Object, e As GridViewEditEventArgs)
        GridView15.EditIndex = e.NewEditIndex
        Me.BindGrid()
    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        GridView15.PageIndex = e.NewPageIndex
        Me.BindGrid()
    End Sub

    
End Class