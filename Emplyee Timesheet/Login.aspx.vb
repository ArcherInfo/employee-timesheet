﻿Imports System.Data.SqlClient
Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim connection As SqlConnection = New SqlConnection(constr)
        Dim command As New SqlCommand("select * from tblLogin where user_Name = @username and password = @password", connection)

        command.Parameters.Add("@username", SqlDbType.VarChar).Value = TextBox1.Text
        command.Parameters.Add("@password", SqlDbType.VarChar).Value = TextBox2.Text

        Dim adapter As New SqlDataAdapter(command)

        Dim table As New DataTable()

        adapter.Fill(table)

        If table.Rows.Count() <= 0 Then

            MsgBox("Username Or Password Are Invalid")

        Else
            Session.Add("Name", TextBox1.Text)
            Session.Add("UserId", 2)

            TextBox2.Text = CType(Session("Name"), String)
            'MsgBox("Login Successfully")
        End If
    End Sub
End Class
