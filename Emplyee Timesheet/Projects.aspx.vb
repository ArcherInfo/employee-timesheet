﻿Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient

Public Class Projects
    Inherits System.Web.UI.Page

    Protected Sub Insert(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Dim project_name As String = txtprojectName.Text
        Dim client_name As String = txtclientName.Text
        Dim query As String = "INSERT INTO tbl_projects VALUES(@pname, @cname)"
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        txtprojectName.Text = ""
        txtclientName.Text = ""
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@pname", project_name)
                cmd.Parameters.AddWithValue("@cname", client_name)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using
    End Sub
End Class