﻿<%@ Page Title="Timesheets" Language="vb" AutoEventWireup="false" MasterPageFile="~/Layout.Master" CodeBehind="Timesheet.aspx.vb" Inherits="Test_application.Timesheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <form id="form4" runat="server">
        <br />
        <table class="auto-style1">
            <tr>
                <td class="auto-style5" style="text-align:center"><b>Fill Timesheet</b></td>
            </tr>
            <tr>
                <td class="auto-style5">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <table class="auto-style3">
                        <tr>
                            <td class="auto-style14">
                                <asp:Label ID="lbl_date" runat="server" Text="Date"></asp:Label>
                            </td>
                            <td class="auto-style14">
                                &nbsp;</td>
                            <td>
                                <asp:TextBox ID="txt_date" type="date" runat="server" Width="340px" Height="28px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style14">
                                &nbsp;</td>
                            <td class="auto-style14">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style14">
                                <asp:Label ID="lbl_time" runat="server" Text="Time"></asp:Label>
                            </td>
                            <td class="auto-style14">
                                &nbsp;</td>
                            <td>
                                <asp:TextBox ID="txt_time" type="time" runat="server" Width="339px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style14">
                                &nbsp;</td>
                            <td class="auto-style14">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style15">
                                <asp:Label ID="Label3" runat="server" Text="Project Name"></asp:Label>
                            </td>
                            <td class="auto-style15">
                                &nbsp;</td>
                            <td class="auto-style16">
                                <asp:DropDownList ID="project_Id" runat="server" DataSourceID="SqlDataSource1" DataTextField="pro_id" DataValueField="pro_id" Width="350px">
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT * FROM [tbl_timesheet]"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">
                                <asp:Button ID="btnAdd3" runat="server" Text="ADD" OnClick="Insert" Width="155px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style15" colspan="3" style="text-align:center"><b>List Of Timesheet</b></td>
                        </tr>
                        <tr>
                            <td class="auto-style15" colspan="3">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style15" colspan="3">
                    <asp:GridView ID="GridView13" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnRowDataBound"
                        DataKeyNames="timesheet_id" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit" PageSize="3" AllowPaging="True" OnPageIndexChanging="OnPaging"
                        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" EmptyDataText="No records has been added."
                        Width="600px" Height="182px" Style="margin-left: 0px" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="timesheet_id" HeaderText="Timesheet Id" InsertVisible="False" ReadOnly="True" SortExpression="timesheet_id" />
                            <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
                            <asp:BoundField DataField="time" HeaderText="Time" SortExpression="time" />
                            <asp:BoundField DataField="pro_id" HeaderText="Project Id" SortExpression="pro_id" />
                            <asp:BoundField DataField="user_id" HeaderText="User Id" SortExpression="user_id" />
                            <asp:CommandField ShowEditButton="True" HeaderText="Actions" ShowDeleteButton="True" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div>
        </div>
    </form>
</asp:Content>
