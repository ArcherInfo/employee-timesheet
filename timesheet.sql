USE [master]
GO
/****** Object:  Database [Timesheet]    Script Date: 01/24/2019 17:42:40 ******/
CREATE DATABASE [Timesheet] ON  PRIMARY 
( NAME = N'Timesheet', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Timesheet.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Timesheet_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Timesheet_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Timesheet] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Timesheet].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Timesheet] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Timesheet] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Timesheet] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Timesheet] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Timesheet] SET ARITHABORT OFF
GO
ALTER DATABASE [Timesheet] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Timesheet] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Timesheet] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Timesheet] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Timesheet] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Timesheet] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Timesheet] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Timesheet] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Timesheet] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Timesheet] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Timesheet] SET  DISABLE_BROKER
GO
ALTER DATABASE [Timesheet] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Timesheet] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Timesheet] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Timesheet] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Timesheet] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Timesheet] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Timesheet] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Timesheet] SET  READ_WRITE
GO
ALTER DATABASE [Timesheet] SET RECOVERY SIMPLE
GO
ALTER DATABASE [Timesheet] SET  MULTI_USER
GO
ALTER DATABASE [Timesheet] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Timesheet] SET DB_CHAINING OFF
GO
USE [Timesheet]
GO
/****** Object:  Table [dbo].[tblLogin]    Script Date: 01/24/2019 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLogin](
	[login_Id] [int] NOT NULL,
	[user_Name] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblLogin] PRIMARY KEY CLUSTERED 
(
	[login_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_user2]    Script Date: 01/24/2019 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_user2](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Middle_Name] [nvarchar](50) NULL,
	[Last_Name] [nvarchar](50) NULL,
	[Gender] [int] NULL,
	[Address] [nvarchar](50) NULL,
	[Dob] [nvarchar](50) NULL,
	[Contact] [nvarchar](max) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Pincode] [nchar](20) NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_user2] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_timesheet]    Script Date: 01/24/2019 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_timesheet](
	[timesheet_id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [nvarchar](50) NULL,
	[time] [nvarchar](50) NULL,
	[pro_id] [int] NULL,
	[user_id] [int] NULL,
 CONSTRAINT [PK_tbl_timesheet] PRIMARY KEY CLUSTERED 
(
	[timesheet_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_projects]    Script Date: 01/24/2019 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_projects](
	[project_id] [int] IDENTITY(1,1) NOT NULL,
	[project_name] [varchar](100) NULL,
	[client_name] [varchar](50) NULL,
 CONSTRAINT [PK_tbl_projects] PRIMARY KEY CLUSTERED 
(
	[project_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_leaves]    Script Date: 01/24/2019 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_leaves](
	[leave_id] [int] IDENTITY(1,1) NOT NULL,
	[leave_date] [date] NULL,
	[leave_type] [nvarchar](50) NULL,
	[user_id] [int] NULL,
 CONSTRAINT [PK_tbl_leaves] PRIMARY KEY CLUSTERED 
(
	[leave_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Holidays]    Script Date: 01/24/2019 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Holidays](
	[first_half] [int] NULL,
	[second_half] [int] NULL,
	[year] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_attendence]    Script Date: 01/24/2019 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_attendence](
	[attendence_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[datetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_attendence] PRIMARY KEY CLUSTERED 
(
	[attendence_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 01/24/2019 17:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Country] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
